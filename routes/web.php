<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\_ExelController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('importExportView');
});

Route::get('importExportView', [_ExelController::class, 'importExportView']);
Route::post('import', [_ExelController::class, 'import'])->name('import');

Route::get('/validation', [_ExelController::class, 'index'])->name('validation');
Auth::routes();

Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

//language
Route::get('/language', [\App\Http\Controllers\LanguagesController::class, 'index'])->name('language');

//countrie
Route::get('/countries', [\App\Http\Controllers\CountriesController::class, 'index'])->name('countries');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/register', [_ExelController::class, 'register'])->name('register');
Route::post('/store', [_ExelController::class, 'store'])->name('store');

Route::get('/insertFTP', [_ExelController::class, 'insertFTP']);

Route::get('/error-sampler/{contentPartnerName}/{key}/missing-value',[\App\Http\Controllers\ErrorFixerController::class,'createFormForMissingValue']);
Route::post('/error-sampler/{contentPartnerName}/{key}/missing-value',[\App\Http\Controllers\ErrorFixerController::class,'postFormDiverseToNode']);
Route::get('/error-sampler/{contentPartnerName}/{key}', [\App\Http\Controllers\ErrorFixerController::class, 'createSelectViewer']);
Route::post('/error-sampler/{contentPartnerName}/{errorType}',[\App\Http\Controllers\ErrorFixerController::class, 'postFormErrorToNode']);

Route::get('/continue',[\App\Http\Controllers\ErrorFixerController::class,'continue']);
