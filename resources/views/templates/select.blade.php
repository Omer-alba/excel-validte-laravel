@foreach($content as $index => $row)
    <tr>
        <th scope="row">{!! $index  !!}</th>
        <td>{!! $row->content->content !!}</td>
        <td>{!! $row->editContent ?? '' !!}</td>
        <td>{!! $row->content->status->successful == true ? '&#x2714;' : '&#x2715;' !!}</td>
        <td>
            <span class='badge badge-secondary '>Sheetname : {!! $row->content->worksheetName !!} |
                    Row number: {!! $row->content->row !!} |
                    Column: {!!  $row->content->cell  !!}
            </span>
        </td>
    </tr>
@endforeach
