@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card bg-light mt-3">
            <div class="card-header">
                Register to Foxxum File Server
            </div>
            <div class="card-body">
                <form action="/store" method="POST" id="js-upload-form" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group" id="contentPartner_group">
                        <label for="contentPartnerName">FTP username</label>
                        <input type="text" style="color: pink;" class="form-control {{ $errors->has('contentPartnerName') ? 'border-danger' : '' }}" name="contentPartnerName" id="contentPartnerName" placeholder="FTP Username" value="{{ old('contentPartnerName') }}">
                        @if($errors->has('contentPartnerName'))
                            <small class="text-danger">{!! $errors->first('contentPartnerName') !!}</small>
                        @endif
                    </div>
                    <div class="form-group" id="contentPartner_group">
                        <label for="contentPartnerPassword">Password</label>
                        <input type="password" class="form-control {{ $errors->has('contentPartnerName') ? 'border-danger' : '' }}" name="contentPartnerPassword" id="contentPartnerPassword" placeholder="Password" value="{{ old('contentPartnerPassword') }}">
                        @if($errors->has('contentPartnerPassword'))
                            <small class="text-danger">{!! $errors->first('contentPartnerPassword') !!}</small>
                        @endif
                    </div>
                    <hr />
                    <div class="form-group" id="contentPartner_group">
                        <label for="contentPartnerHost">Select your Server</label>
                        <select class="form-control" id="contentPartnerHost" name="contentPartnerHost">
                            @foreach($serverNameList as $serverName)
                                <option value={{ $serverName["host"] }}>{{ $serverName["host"] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">regist</button>
                </form>
            </div>
        </div>
    </div>
@endsection
