@extends('layouts.app')
@section('content')
<style>
    .files input {
        outline: 2px dashed #92b0b3;
        outline-offset: -10px;
        -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
        transition: outline-offset .15s ease-in-out, background-color .15s linear;
        padding: 120px 0px 85px 35%;
        text-align: center !important;
        margin: 0;
        width: 100% !important;
    }
    .files input:focus{     outline: 2px dashed #92b0b3;  outline-offset: -10px;
        -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
        transition: outline-offset .15s ease-in-out, background-color .15s linear; border:1px solid #92b0b3;
    }
    .files{ position:relative}
    .files:after {  pointer-events: none;
        position: absolute;
        top: 60px;
        left: 0;
        width: 50px;
        right: 0;
        height: 56px;
        content: "";
        background-image: url(https://image.flaticon.com/icons/png/128/109/109612.png);
        display: block;
        margin: 0 auto;
        background-size: 100%;
        background-repeat: no-repeat;
    }
    .color input{ background-color:#f1f1f1;}
    .files:before {
        position: absolute;
        bottom: 5px;
        left: 0;
        pointer-events: none;
        width: 100%;
        right: 0;
        height: 57px;
        content: " or drag it here. ";
        display: block;
        margin: 0 auto;
        color: #2ea591;
        font-weight: 600;
        text-transform: capitalize;
        text-align: center;
    }

    .alert,.alert_php {
        padding: 20px;
        background-color: #f44336; /* Red */
        color: white;
        margin-bottom: 15px;
    }

    .alert{
        display: none;
    }

    /* The close button */
    .closebtn {
        margin-left: 15px;
        color: white;
        font-weight: bold;
        float: right;
        font-size: 22px;
        line-height: 20px;
        cursor: pointer;
        transition: 0.3s;
    }

    /* When moving the mouse over the close button */
    .closebtn:hover {
        color: black;
    }

</style>
<div class="container">
    <div class="card bg-light mt-3">
        <div class="card-header">
            Foxxum Excel file Validator
        </div>
        <div class="card-header">
            <strong>Upload Files</strong>
            <small>Bootstrap files upload</small>
        </div>
        <div class="card-body">
            <div class="{{  isset($message) ? "alert_php" : "alert"}}">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">x</span>
                @isset($message)
                    <small>{{ $message }}</small>
                @endisset
            </div>
            <form action="http://127.0.0.1:3001/upload" method="POST" id="js-upload-form" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <input type="hidden" class="form-control" id="pathname" name="pathname" id="pathname" value="">
                </div>
                <div class="form-group" id="contentPartner_group">
                    <label for="contentPartnerName">Contentpartner <small>FTP Username</small></label>
                    <input type="text" class="form-control" name="contentPartnerName" id="contentPartnerName" placeholder="FTP Username" value="{{ old('contentPartnerName') }}">
                </div>
                <div class="form-group" id="contentPartner_group">
                    <label for="contentPartnerPassword">Contentpartner <small>FTP password</small></label>
                    <input type="password" class="form-control" name="contentPartnerPassword" id="contentPartnerPassword" placeholder="Password" value="{{ old('contentPartnerPassword') }}">
                </div>
                <hr />
                <h4>Select files from your computer</h4>
                <div class="form-group files" id="dropContainer">
                    <label>Upload Your File</label>
                    <input type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required name="file" id="js-upload-files" class="form-control">
                </div>
                <br>
                <button type="submit" class="btn btn-success">Import Excel-Data</button>
            </form>
        </div>
    </div>
</div>

<script>
    !function (){
        let pathname = document.querySelector('#pathname');
        pathname.value = location.origin;

        if (location.search){
            const urlParams = new URLSearchParams(location.search);
            const message = urlParams.get('message');
            // check if missing name or password
            let content_group = undefined;
            if (message.includes('Password')){
                content_group = document.querySelectorAll('#contentPartner_group')[1];
            } else if(message.includes('file')){
                const alert = document.querySelector('.alert');
                alert.style.display = "block";
                alert.innerHTML += '<small>'+ message + '</small>';
                return false;
            } else {
                content_group = document.querySelectorAll('#contentPartner_group')[0];
            }

            content_group.innerHTML += `<small style="color:red">${message}</small>` || `<small>empty</small>`;
        }

    }();

    window.addEventListener("dragover",function(e){
        e = e || event;
        e.preventDefault();
    },false);
    window.addEventListener("drop",function(e){
        e = e || event;
        e.preventDefault();
    },false);

    const dropContainer = document.querySelector('#dropContainer');
    const fileInput = document.querySelector('#js-upload-files');
    dropContainer.ondragover = dropContainer.ondragenter = function(evt) {
        evt.preventDefault();
    };

    dropContainer.ondrop = function(evt) {
        // pretty simple -- but not for IE :(
        fileInput.files = evt.dataTransfer.files;
        // If you want to use some of the dropped files
        const dT = new DataTransfer();
        dT.items.add(evt.dataTransfer.files[0]);
        dT.items.add(evt.dataTransfer.files[3]);
        fileInput.files = dT.files;

        evt.preventDefault();
    };
</script>
@endsection
