@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/editViaSelectView.css') }}" rel="stylesheet">
    <div id="header"></div>
    <a href="#header" class="back-to-up">
        <span>&#x21ea;</span>
    </a>
    <form action='/error-sampler/{!! $contentPartnerName !!}/diverse/missing-value' method='post'>
        @csrf
        <div class="container">
            <nav class="navbar navbar-light bg-light">
                @foreach($keys as $key)
                    <a class="navbar-brand" href="#{!! $key !!}">jump to <u>{!! $key !!}</u></a>
                @endforeach
            </nav>
            @isset($data->number)
                <div class="modal-dialog" id="number">
                    <h6>Number</h6>
                </div>
                @foreach($data->number->elements as $key => $element)
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class='form-group'>

                                <div class="modal-header">
                                    <label for='invalidUrl'>
                                        Please refill the value for <u>{!! $element->content->name !!}</u>
                                        <span class='badge badge-secondary'>
                                        Sheetname : {!! $element->content->worksheetName !!} |
                                        Row number: {!! $element->content->row !!} |
                                        Column: {!!  $element->content->cell  !!}
                                    </span>
                                    </label>
                                </div>

                                <div class="modal-body">
                                    <input type='number' class='form-control' id='invalidUrl'
                                           name='number-value-{!! $key !!}'
                                           aria-describedby='invalid url'
                                           placeholder='{!! $element->content->content !!}'
                                    >
                                </div>

                                <div class="modal-footer">
                                    <span
                                          class='form-text text-muted'>{{ $element->content->description}}</span>
                                    <small class='form-text text-muted'>this value is required, and can't be
                                        empty!</small>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
                <hr>
            @endisset

            @isset($data->text)
                    <div class="modal-dialog" id="text">
                        <h6>Text</h6>
                    </div>
                @foreach($data->text->elements as $key => $element)
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class='form-group'>

                                <div class="modal-header">
                                    <label for='invalidUrl'>
                                        Please refill the value for <u>{!! $element->content->name !!}</u>
                                        <span class='badge badge-secondary'>
                                                    Sheetname : {!! $element->content->worksheetName !!} |
                                                    Row number: {!! $element->content->row !!} |
                                                    Column: {!!  $element->content->cell  !!}
                                                </span>
                                    </label>
                                </div>

                                <div class="modal-body">
                                    <input type='text' class='form-control' id='invalidUrl'
                                           name='text-value-{!! $key !!}'
                                           aria-describedby='invalid url'
                                           placeholder='{!! $element->content->content !!}'
                                    >
                                </div>

                                <div class="modal-footer">
                                    <span
                                          class='form-text text-muted'>{{ $element->content->description}}</span>
                                    <small class='form-text text-muted'>
                                        this value is required, and can't be empty!
                                    </small>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
                <hr>
            @endisset

            @isset($data->language)
                    <div class="modal-dialog" id="language">
                        <h6>Language</h6>
                    </div>
                @foreach($data->language->elements as $index => $element)
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class='form-group from-selecter'>
                                <div class="modal-header">
                                    <label for='emptyLanguage'>
                                        Please refill the value for <u>{!! $element->content->name !!}</u>
                                        <span class='badge badge-secondary'>
                                            Sheetname : {!! $element->content->worksheetName !!} |
                                            Row number: {!! $element->content->row !!} |
                                            Column: {!!  $element->content->cell  !!}
                                        </span>
                                    </label>
                                </div>

                                <div class="modal-body">
                                    <select class='custom-select'
                                            name='selected-language-{!! $index !!}[]' !!}
                                            id='inputGroupSelect02'
                                            data-live-search='true' multiple>
                                        <option value='' disabled>choose..</option>

                                        @foreach($data->language->resolve as $index => $require)
                                            <option value='{!! $require->iso_2 !!}'>{!! $require->name !!}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="modal-footer">
                                    <small class='form-text text-muted'>this value is required, and can't
                                        be
                                        empty!
                                    </small>
                                    <small class='form-text text-muted'>
                                        to choose mutiple lamguages, please hold 'cmd' or 'str' and click choose the
                                        next
                                        language!
                                    </small>
                                </div>

                                <div class="modal-footer">
                                    <div class='selected-viewer-container d-none'>
                                        <h6>Ausgewählte urls</h6>
                                        <div>
                                            <ol class='select-view'></ol>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
                <hr>
            @endisset

            @isset($data->files)
                <div class="modal-dialog" id="files">
                    <h6>Files</h6>
                </div>
                @foreach($data->files->elements as $index => $element)
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class='form-group'>
                                <div class="modal-header">
                                    <label for='emptyLanguage'>
                                        Please refill the value for <u>{!! $element->content->name !!}</u>
                                        <span class='badge badge-secondary'>
                                                    Sheetname : {!! $element->content->worksheetName !!} |
                                                    Row number: {!! $element->content->row !!} |
                                                    Column: {!!  $element->content->cell  !!}
                                                </span>
                                    </label>
                                </div>

                                <div class="modal-body">
                                    <select class='custom-select'
                                            name='selected-value-{!! $index !!}{!! $errorType === 'language' ? '[]' : '' !!}'
                                            id='inputGroupSelect02'
                                            data-live-search='true'>
                                        <option value=''>choose..</option>

                                        @foreach($data->files->resolve as $index => $require)
                                            <option value='{!! $require !!}'>{!! $require !!}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="modal-footer">
                                    <small class='form-text text-muted'>
                                        this value is required, and can't beempty!
                                    </small>

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endisset
            <div class="modal-dialog">
                <button type='submit' class='btn btn-primary'>Submit</button>
            </div>
            <script src="{{ asset('js/editViaSelectView.js') }}" defer></script>
        </div>
@endsection
