@extends('layouts.app')

@section('content')
    @if(isset($message))
        <h1>{!! $message !!}</h1>
    @endif
        @include('templates.tableHeader')
        @foreach($data as $key => $content)
            @if(isset($content))
                @include('templates.select',['content' => $content->elements])
            @endif
            <td></td>
            <td></td>
            <td>
                <center><h4>{!! $key !!}</h4></center>
            </td>
            <td></td>
            <td></td>
        @endforeach
        @include('templates.tableFooter')
@endsection
