@extends('layouts.app')

@section('content')
    <main class="sm:container sm:mx-auto sm:mt-10">

        @foreach($errors as $error)
            <div class="alert {{$error->class}}" role="alert">
                {!! $error->message !!}
            </div>
        @endforeach
        {{ $filename ?? "unkown file name!" }}
    </main>
@endsection
