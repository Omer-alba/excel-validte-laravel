@extends('layouts.app')

@section('content')
    @if(isset($message))
        <h1>{!! $message !!}</h1>
    @endif

    @if(isset($data))
        @include('templates.tableHeader')
        @include('templates.select',['content' => $data])
        @include('templates.tableFooter')
    @endif
@endsection
