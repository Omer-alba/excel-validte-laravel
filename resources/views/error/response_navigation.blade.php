<div>
    <div class='btn-group btn-group-lg' role='group'>
        <a class="btn btn-secondary" href={!! config('url.node.url') !!}xlxs-download style='color: #fff; width: 100%; text-align: center;'>
            <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor'
                 class='bi bi-download' style='margin-right: 25px' viewBox='0 0 16 16'>
                <path
                    d='M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z'/>
                <path
                    d='M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z'/>
            </svg>
            <small>Download <u>edited</u> {!! $data[0]->filename ?? $filename !!} file</small>
        </a>
        <form class="btn btn-secondary" action={!! config('url.node.url') !!}continue method="post">
            <input style="border: none; background-color: transparent; color: white" type="submit" value="apply & continue">
            <input type="text" name="errorType" value="{!! $errorType !!}" hidden>
        </form>
        <a class="btn btn-secondary" href="/validation">cancel & continue</a>
    </div>
</div>
