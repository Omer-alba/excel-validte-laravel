@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/editViaSelectView.css') }}" rel="stylesheet">

    <form action='/error-sampler/{!! $contentPartnerName !!}/{!! $url !!}' method='post'>
        @csrf
        <div class="container">
            @foreach($data as $index => $element)
                <div class="modal-dialog">

                    <div class="modal-content">
                        <div class="modal-header">
                            <div class='form-group'>

                                <label for='invalidUrl'>
                                    invalid Url
                                    <span class='badge badge-secondary '>Sheetname : {!! $element->content->worksheetName !!} |
                                    Row number: {!! $element->content->row !!} |
                                    Column: {!!  $element->content->cell  !!}
                                </span>
                                </label>
                            </div>
                        </div>

                        <div class="modal-body">
                            <input type='text' class='form-control' id='invalidUrl' aria-describedby='invalid url'
                                   placeholder='{!! $element->content->content !!}' readonly
                            >
                            <small id='text' class='form-text text-muted'>
                                this url is invalid, pls choose one of from the select box
                            </small>
                        </div>

                        <div class="modal-footer">
                            <div class='form-group from-selecter'>
                                <div class='input-group mb-3'>

                                    <select class='custom-select'
                                            name='selected-value-{!! $index !!}{!! in_array($errorType,['language','country']) ? '[]' : '' !!}'
                                            id='inputGroupSelect02'
                                            data-live-search='true' {{ in_array($errorType,['language','country']) ? 'multiple' : ''}}
                                    >
                                        <option value='' {{ $errorType === 'language' ? '' : 'selected'}} disabled>
                                            choose..
                                        </option>
                                        @if($errorType === 'invalid')

                                            @foreach($requires as $index => $require)
                                                <option value='{!! $require !!}'> {!! $require !!}</option>
                                            @endforeach

                                        @elseif($errorType === 'language' || $errorType === 'country')

                                            @foreach($requires as $index => $require)
                                                <option
                                                    value='{!! $require->iso_2 !!}' {!! in_array($require->iso_2,explode(',',strtolower($element->content->content))) ? 'selected' : '' !!}>
                                                    {!! $require->name !!}
                                                </option>
                                            @endforeach
                                            <script src="{{ asset('js/editViaSelectView.js') }}" defer></script>
                                        @endif
                                    </select>

                                    <div class='input-group-append'>
                                        <label class='input-group-text' for='inputGroupSelect02'>urls</label>
                                    </div>

                                </div>
                                @if(in_array($errorType,['language','country']))
                                    <small id='text' class='form-text text-muted'>
                                        to <b>choose mutiple lamguages</b>, please hold 'cmd' or 'str' and click choose
                                        the
                                        next language!
                                    </small>
                                @endif

                                <div class='selected-viewer-container d-none'>
                                    <h4>Ausgewählte urls</h4>
                                    <div>
                                        <ol class='select-view'></ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="modal-dialog">

            </div>
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type='submit' class='btn btn-primary'>Submit</button>
                        <a href="/validation" class='btn btn-secondary'>Cancel</a>
                </div>
            </div>
        </div>
    </form>
@endsection
