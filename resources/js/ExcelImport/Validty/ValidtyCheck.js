const RequiredFields = require('./RequiredFields');

class ValidtyCheck {
    constructor(data) {
        this.data = data
        this.required = new RequiredFields();
    }

    run(){
        for (const argument of this.data) {
            if (!argument.type){ continue }
            this.required.isRequired(argument);
        }
        return true;
    }
}

module.exports = ValidtyCheck;
