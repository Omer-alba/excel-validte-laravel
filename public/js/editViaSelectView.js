/**
 * This File belongs to the View (error.editViaSelect.blade.php)
 * ------------------------------------------------------------
 */

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

const selectContainer = document.querySelectorAll('.from-selecter');

const showSelectedElementsFunction = function (event,container) {
    // query
    const selectedViewContainer = container.querySelector('.selected-viewer-container');
    const selectedValueElement = container.querySelector('.select-view');
    //modify query
    selectedValueElement.innerHTML = '';
    const isInvisible = selectedViewContainer.classList.contains('d-none')
    const selectedOptions = Array.prototype.slice.call(event.target.selectedOptions);

    if (isInvisible) {
        selectedViewContainer.classList.remove('d-none');
    }

    const newSelector = selectedOptions.filter(onlyUnique);
    for (const selectedOption of newSelector) {
        if(selectedOption === 'choose')  continue;
        selectedValueElement.innerHTML += `<li>${selectedOption.text}</li>`;
    }
}

for (const container of selectContainer) {
    container.querySelector('.custom-select').addEventListener('change', (event)=>{
        showSelectedElementsFunction(event,container);
    });
}
