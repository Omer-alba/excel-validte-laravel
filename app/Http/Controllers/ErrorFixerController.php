<?php

namespace App\Http\Controllers;

use App\Models\ErrorSampler;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class ErrorFixerController extends Controller
{
    public function createSelectViewer(Request $request, $contentPartnerName, $key)
    {
        $queryErrorSampler = ErrorSampler::where('name', '=', $contentPartnerName . '-' . $key)
            ->first();

        if ($key === 'spelling') {
            return $this->postFormErrorToNode($request, $contentPartnerName, $key);
        }

        return view('error.editViaSelect')
            ->with([
                'data' => json_decode($queryErrorSampler->content),
                'errorType' => $key,
                'requires' => json_decode($queryErrorSampler->requires),
                'url' => $queryErrorSampler->url,
                'contentPartnerName' => $contentPartnerName
            ]);
    }

    public function createFormForMissingValue(Request $request, $contentPartnerName, $key)
    {
        $queryErrorSampler = ErrorSampler::where('name', '=', $contentPartnerName . '-' . $key)
            ->first('content');

        $queryErrorSamplerKeys = json_decode($queryErrorSampler->content);

        return view('error.diverseForm')->with([
            'data' => json_decode($queryErrorSampler->content),
            'errorType' => $key,
            'url' => $queryErrorSampler->url,
            'contentPartnerName' => $contentPartnerName,
            'keys' => array_keys((array)$queryErrorSamplerKeys),
        ]);
    }

    public function postFormErrorToNode(Request $request, $contentPartnerName, $errorType)
    {
        $queryErrorSampler = ErrorSampler::where('name', '=', $contentPartnerName . '-' . $errorType)
            ->first();

        $url = config('url.node.url') . config('url.node.write') . $queryErrorSampler['url'];

        $sampler = array();
        $sampler['data'] = $queryErrorSampler["content"];
        $sampler['errorType'] = $queryErrorSampler['url'];
        $sampler['contentPartnerName'] = $contentPartnerName;

        if (in_array($errorType,['language','country'])) {
            $objectArraysToString = array();

            foreach ($request->all() as $key => $node) {
                if ($key === '_token') continue;
                $objectArraysToString[$key] = implode(',', $node);
            }

            $sampler['toReplace'] = json_encode($objectArraysToString);

        } elseif (in_array($errorType,['invalid'])) {
            $sampler['toReplace'] = json_encode($request->all());
        }

        $httpResponse = Http::post($url, $sampler);

        sleep(1);
        if ($httpResponse->ok() == true) {
            $queryErrorSampler = ErrorSampler::where('name', '=', $contentPartnerName . '-' . $errorType)
                ->first();

            $contentPartnerInfo = json_decode(file_get_contents('http://127.0.0.1:3001/filename'));

            return view('error.editedResponse')
                ->with([
                    'data' => json_decode($queryErrorSampler['content']),
                    'contentPartnerName' => $contentPartnerName,
                    'errorType' => $queryErrorSampler['url'],
                    'filename' => $contentPartnerInfo->filename
                ]);
        }
    }

    public function postFormDiverseToNode(Request $request, $contentPartnerName, $errorType)
    {
        $queryErrorSampler = ErrorSampler::where('name', '=', $contentPartnerName . '-' . $errorType)
            ->first();

        $url = '127.0.0.1:3001/writeExcel/' . $queryErrorSampler['url'];

        $array = [];
        foreach ($request->all() as $key => $element) {
            if ($key === '_token') continue;

            if (gettype($element) == 'array') {
                $array[$key] = implode(',', $element);
            } else {
                $array[$key] = $element;
            }

        }

        $httpResponse = Http::post($url, [
            'toReplace' => json_encode($array),
            'data' => $queryErrorSampler['content'],
            'errorType' => $queryErrorSampler['url'],
            'contentPartnerName' => $contentPartnerName,
        ]);

        sleep(1);
        if ($httpResponse->ok() == true) {
            // to detect any changes!
            $queryErrorSampler = ErrorSampler::where('name', '=', $contentPartnerName . '-' . $queryErrorSampler['url'])
                ->first();

            $contentPartnerInfo = file_get_contents('http://127.0.0.1:3001/filename');
            $contentPartnerInfo = json_decode($contentPartnerInfo);

            session(['user' => $contentPartnerInfo->contentPartnerName]);
            session(['filename' => $contentPartnerInfo->filename]);

            return view('error.diverseResponse')
                ->with([
                    'data' => (array)json_decode($queryErrorSampler['content']),
                    'contentPartnerName' => $contentPartnerName,
                    'errorType' => $queryErrorSampler['url'],
                    'filename' => $contentPartnerInfo->filename
                ]);
        }
    }

    public function continue(Request $request)
    {
        dump($request->session()->get('user'));

        Http::post('http://127.0.0.1:3001/continue', [
            'contentPartnerName' => $request->session()->get('user'),
            'contentPartnerName' => $request->session()->get('user'),
            'filename' => $request->session()->get('filename'),
        ]);
        // erstelle eine http post zu node
        //verwende die hinterlegte node datei!
        // dann redirecte zu validation  und diese übernimmt den rest!
    }
}

/**
 * $postdata = http_build_query(
 * array(
 * 'var1' => 'some content',
 * 'var2' => 'doh'
 * )
 * );
 *
 * $opts = array('http' =>
 * array(
 * 'method'  => 'POST',
 * 'header'  => 'Content-Type: application/x-www-form-urlencoded',
 * 'content' => $postdata
 * )
 * );
 *
 * $context  = stream_context_create($opts);
 *
 * $result = file_get_contents('http://example.com/submit.php', false, $context);
 */
