<?php

namespace App\Http\Controllers;

use App\Models\Ftp_accesses;
use Illuminate\Http\Request;

//own
use Maatwebsite\Excel\Facades\Excel;
use App\Services\ExcelParserService;
use App\Services\ErrorAssemble\ErrorMsg;
use Illuminate\Support\Facades\Crypt;
use SSH;
use App\Services\InsertIntoFtp_accesses_table;
use App\Models\ErrorSampler;
use Illuminate\Support\Facades\Http;


global $errorMsg;
$errorMsg = new ErrorMsg();

class _ExelController extends Controller
{
    public function importExportView()
    {
        return view('exel.import');
    }

    public function import()
    {
        return view('exel.import');
    }

    public function index()
    {
        global $errorMsg;
        //http request GET
        sleep(1);
        $xml = file_get_contents(config('url.node.url').'result');
        $xml_data = file_get_contents(config('url.node.url').'filename');

        if (!$xml || !$xml_data){
            return view('error.failToLoad');
        }

        $xml = json_decode($xml, true);
        $xml_data = json_decode($xml_data, true);

        $excelparser = new ExcelParserService($xml);
        // save file attributes (Name, file sever name and password)
        $errorMsg->filename = $xml_data['filename'];
        $errorMsg->contentPartnerName = $xml_data['contentPartnerName'];
        $errorMsg->contentPartnerPassword = $xml_data['contentPartnerPassword'];

        // begin with the xlsx validation
        $excelparser->run();

        // insert error Collector to database
        foreach ($errorMsg->collector as $key => $collector) {
            ErrorSampler::updateOrCreate(
                ['name' => $errorMsg->contentPartnerName . '-' . $key],
                ['content' => json_encode($errorMsg->collector[$key])]
            );
        }

        if(count($errorMsg->diverseCollector) > 0){
            ErrorSampler::updateOrCreate(
                ['name' => $errorMsg->contentPartnerName . '-diverse'],
                ['url' => 'diverse', 'content' => json_encode($errorMsg->diverseCollector)]
            );
        }

        if ($errorMsg->connectionFailed) {
            return view('exel.import')
                ->with(['message' => 'ftp-username or password is wrong, please give it an new try']);
        }

        if ($errorMsg->userNotExists) {
            return view('exel.import')
                ->with(['message' => 'ftp-username is not exists']);
        }

        $error = $errorMsg->showErrorMessageToClient();
        return view('error.index')
            ->with(['error' => $error]);
    }

    public function register()
    {
        $serverNameList = [
            ['host' => 'host.de',
                'name' => 'host',],
            ['host' => 'web.de',
                'name' => 'web',]
        ];

        return view('exel.register')
            ->with(['serverNameList' => $serverNameList]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'contentPartnerName' => 'required|min:3',
            'contentPartnerPassword' => 'required|min:4',
            'host' => 'min:4'
        ]);

        $username = $request->get('contentPartnerName');
        $password = $request->get('contentPartnerPassword');
        $host = $request->get('contentPartnerHost');

        $store = new Ftp_accesses([
            'user' => $username,
            'password' => Crypt::encryptString($password),
            'host' => $host,
            'port' => 22
        ]);

        $store->save();
        /*
        system("sudo useradd  $username  -d /home/$username -s /usr/sbin/nologin -m");
        system("sudo usermod $username -g sftpaccess");
        system("sudo chown root:root /home/$username/");
        system("sudo mkdir /home/$username/uploads");
        system("sudo chown $username:sftpaccess /home/$username/uploads/");
        system("sudo echo $username:$password | chpasswd");
        system("sudo mv /home/$username /home_extend/");
        system("sudo ln -s /home_extend/$username /home/$username");
        system("sudo ln -s /home_extend/$username/uploads/ /var/www/html/$username");
        */
    }

    public function insertFTP()
    {
        $insertFTP = new InsertIntoFtp_accesses_table();
        $insertFTP->insert();
    }

    // delete me!!!!

    /**
     * TODO remove the postError!;
     */
    public function postError(Request $request, $errorType)
    {
        $solveError = ErrorSampler::where('name', '=', $errorType)
            ->first('content');

        dd($solveError['content']);
        $url = json_decode($solveError['content'])[0]->url;

        $httpResponse = Http::post($url, [
            'data' => $solveError,
        ]);

        sleep(1);

        if ($httpResponse->failed()) {
            return view('error.editedResponse')
                ->with(['message' => 'something got wrong, pls try again']);
        }

        if ($httpResponse->successful()) {
            $queryErrorSampler = ErrorSampler::where('name', '=', $errorType)
                ->first('content');

            return view('error.editedResponse')
                ->with(['data' => json_decode($queryErrorSampler['content'])]);
        }

    }

}
