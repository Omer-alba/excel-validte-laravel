<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;


class UsersImport implements WithMultipleSheets, WithEvents
{
    use Importable, RegistersEventListeners;

    protected static $allSheets;
    protected static $importSheetsArray;

    public static function beforeImport(BeforeImport $event)
    {
        self::$allSheets = $event->reader->getAllSheets();
        foreach (self::$allSheets as $sheet) {
            dump($sheet);
            self::$importSheetsArray[$sheet->getTitle()] = new FirstSheetImport();
        }
    }

    public static function afterImport(AfterImport $event)
    {
        echo "after Import!";
    }

    public static function beforeSheet(BeforeSheet $event)
    {
        echo "before sheet!";
    }

    public static function afterSheet(AfterSheet $event)
    {
        echo "After sheet!";
    }

    public function sheets(): array
    {
        echo "hier kommt der importSheetsArray";
        return [
             1 => new FirstSheetImport()
        ];
    }

}
