<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;

//own import
use App\Services\ExcelParserService;

class FirstSheetImport implements ToCollection
{

    public function collection(Collection $row)
    {
        /*
         * ####### valid form to handle and to work with!  #######
         * {
         *      type: optional/required
         *      name: string
         *      description: text
         *      value: string/text
         * }
         *  ####### validation  #######
         * 1.) check type
         * 1.1) is required -> check value : continue
         * 1.2) validation fail? -> save error to variable
         *
         *  ####### Error variable  #######
         * [
         *  {whereError: $name, howToFix: $tippFromValiation function, possiblities: optional/array}
         * ]
         * */


        //parse!
        $excelParserModul = new ExcelParserService();
        $assembledSheet = $excelParserModul->parseToValidFormat($row);
        $validSheet = $excelParserModul->defineFormat($assembledSheet);

        return true;
    }

}

