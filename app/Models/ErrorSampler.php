<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErrorSampler extends Model
{
    use HasFactory;
    protected $table = 'error_sampler';
    protected $fillable = [
        'name',
        'content',
        'requires',
        'url'
    ];
}
