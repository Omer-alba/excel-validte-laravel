<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ftp_accesses extends Model
{
    use HasFactory;
    protected $fillable = ['user','password','host','port'];

}
