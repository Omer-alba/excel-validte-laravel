<?php
namespace App\Services\Rows;
use App\Services\RequireTableInformationService;

class CoulmnRule {
    public function __construct(){
        //
    }

    public static function getRule($name){
        switch ($name){
         case 'allowmix':
             return [
                 'type' => 'regex',
                 'rule' => '/^yes$|^no$/i',
                 'default' => 'Yes'
             ];
             break;
            case 'playouttype':
                return [
                 'type' => 'regex',
                 'rule' => '/^linear$|^Nonlinear$|^linear & Nonlinear$/i',
                 'default' => 'Linear & Nonlinear'
             ];
            break;
         case 'title':
         case 'category':
             return [
                 'type' => '!regex',
                 'rule' => '/^([A-Z ]*)$|^[\s\S]{100,}/',
                 'default' => false
             ];
             break;
         case 'description':
         case 'seriesdescription':
             return [
                 'type' => '!regex',
                 'rule' => '/^[\s\S]{999,}/i',
                 'default' => false,
             ];
             break;
         case 'language':
             return [
                 'type' => 'if',
                 'rule' => RequireTableInformationService::handleTableRule($name),
                 'default' => false,
             ];
             break;
         case 'videofile':
             return [
                 'type' => 'url',
                 'rule' => ' ',
                 'default' => false,
             ];
             break;
        case 'blockedcountries':
        case 'country':
        case 'allowedcountries':
             return [
                 'type' => 'if',
                 'rule' => RequireTableInformationService::handleTableRule($name),
                 'default' => true,
             ];
             break;
            case 'ratingtype':
                return [
                    'type' => '!regex',
                    'rule' => '/[0-9]/',
                    'default' => false,
                ];
                break;
            case 'rating':
                return [
                    'type' => 'regex',
                    'rule' => '/^adult$|^nonadult$|(.{1,})/i',
                    'default' => false
                ];
                break;
         case 'adbreaks':
             return [
                 'type' => '!regex',
                 'rule' => "/[*|\":<>[\]{}`\\()';@&$^_-a-z]/i",
                 "default" => false,
             ];
             break;
         case 'year':
             return [
                 'type' => 'regex',
                 'rule' => '/(^[0-9]{1,2})(\/{1})(([0-9]{1,2}))(\/{1})(([0-9]{4,4})$)|^[0-9]{4,4}$/',
                 'default' => false
             ];
             break;
        case 'subtitles':
             return [
                 'type' => '!regex',
                 'rule' => '/[a-z]*(.srf|.vtt)/i',
                 'default' => false
             ];
             break;
        case 'thumbnail':
        case 'seriesthumbnail':
            return [
                'type' => 'url',
                'rule' => ' ',
                'default' => false
            ];
            break;
         case 'keywords':
             return [
                 'type' => 'if',
                 'rule' => 'enumeration',
                 'default' => false
             ];
        case 'avabilablestart':
        case 'availableend':
             return [
                 'type' => 'regex',
                 'rule' => '/(^[0-9]{1,2})(\/{1})(([0-9]{1,2}))(\/{1})(([0-9]{4,4})$)/',
                 'default' => false
             ];
             break;
         case 'seriestitle':
             return [
                 'type' => '!regex',
                 'rule' => '/(?:[A-Z]{2,})|^[a-z0-9öüäß ]{49,}|[_]/',
                 'default' => false
             ];
             break;
        case 'artist':
        case 'producers':
        case 'directors':
        case 'actors':
        case 'writers':
            return [
                'type' => '!regex',
                'rule' => '/^[\s\S]{1023,}|[\/\\:;\_<>]/i',
                'default' => false
            ];
            break;
        case 'seriesseasonnumber':
        case 'runtime':
             return [
                 'type' => 'regex',
                 'rule' => '/^[0-9]*$/',
                 'default' => false
             ];
             break;
        case 'seriesepisodenumber':
            return [
                'type' => 'regex',
                'rule' => '/^[0-9]*$|^[0-9]*-[0-9]*$/',
                'default' => false
            ];
            break;
            case 'guid':
            case 'imdbid':
            case 'seriesid':
                return [
                    'type' => 'regex',
                    'rule' => '/.*/',
                    'default' => false
                ];
                break;
        }
        return false;
    }
}
