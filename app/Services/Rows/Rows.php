<?php

namespace App\Services\Rows;

class Rows{
    public function __construct()
    {
        //
    }

    public static function requiredRows(){
        return ([
            'Movie' => ['guid','allowMix','playoutType','language','title','description','videoFile','allowedCountries','blockedCountries','adBreaks','year','runtime','category','subTitles','thumbnail','country','keywords','ratingType','rating','directors','actors','writers','producers','imdbId','avabilableStart','availableEnd'],
            'TV' => ['seriesId','seriesTitle','seriesDescription','seriesThumbnail','seriesSeasonNumber','seriesEpisodeNumber','guid','allowMix','playoutType','language','title','description','videoFile','allowedCountries','blockedCountries','adBreaks','year','runtime','category','subTitles','thumbnail','country','keywords','ratingType','rating','directors','actors','writers','producers','imdbId','avabilableStart','availableEnd'],
            'Music' => ['guid','allowMix','playoutType','language','title','description','videoFile','allowedCountries','blockedCountries','adBreaks','year','runtime','category','subTitles','thumbnail','country','keywords','ratingType',"rating","artist","avabilableStart","availableEnd"],
            "Other" => ["guid","allowMix","playoutType","language","title","description","videoFile","allowedCountries","blockedCountries","adBreaks","year","runtime","category","subTitles","thumbnail","country","keywords","ratingType","rating","avabilableStart","availableEnd"]
        ]);
    }
}
