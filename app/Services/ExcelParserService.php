<?php

namespace App\Services;

//###### Service ######
use App\Models\Ftp_accesses;
use App\Services\ExcelControlValidator;
use App\Http\Controllers\_ExelController;

//filesystem
use App\Services\Rows\CoulmnRule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;


class ExcelParserService
{
    public function __construct($data)
    {
        $this->data = $data;
        $this->handlerService = new ExcelControlValidator();
        $this->filename = '';
        $this->allFile = '';
    }


    public function run()
    {
        global $errorMsg;

        //are all needed column avaible?
        $fail = $this->handlerService->isRowAvaible((object)$this->data);

        if ($fail) {
            return $fail;
        }

        //connect to db and request table
        $user = Ftp_accesses::where('user', '=', $errorMsg->contentPartnerName)->first();

        if ($user === null) {
            return $errorMsg->userNotExists = true;
        }

        // connect to file Server
        if (Crypt::decrypt($user->password) == $errorMsg->contentPartnerPassword) {
            $disk = self::createConnectionToFileServer($errorMsg->contentPartnerName, $errorMsg->contentPartnerPassword, $user->host);
            $this->allFile = $disk->allFiles();
            $errorMsg->allFile = $this->allFile;
        } else {
            return $errorMsg->connectionFailed = true;
        }

        foreach ($this->data as $element) {
            //contine if empty column... Shows only the name of the Sheet.
            if (!$element['name'] && !$element['type']) {
                continue;
            }

            //check if type(Require or Optional is avaible)
            if (strtoupper($element['type']) !== 'OPTIONAL' && strtoupper($element['type']) !== 'REQUIRED') {
                $errorMsg->assembleErrors((object)$element, 'Type is missing');
            }

            //extends the $element object with rules, condition and default.
            $obj_element = (object)self::attendRuleForExcelRow($element);

            //if type is null return!
            if ($obj_element->type === null) {
                continue;
            }

            try {
                // loob for all values!
                foreach ($obj_element->valueList as $value) {
                    $element = self::extractValueFromData($obj_element, $value);

                    if (strtolower($element->type) === 'required')
                        $this->handlerService->isRequiredSetted($element, $this->allFile);
                    else
                        $this->handlerService->isOptionalSetted($element, $this->allFile);
                }
            } catch (\Exception $e) {
                echo 'error loob for all values';
                dump($obj_element);
                dump($e->getMessage());
                dump($e->getFile());
                dump($e->getLine());
                $errorMsg->assembleErrors($obj_element, 'fatal');
            }
        }
        return false;
    }


    public static function attendRuleForExcelRow($element)
    {
        // extends the $element object with the rules, condition, and default.
        // !All Rules are in the Folder App/Services/Rows/ColumnRule.php
        $rule = CoulmnRule::getRule(strtolower($element['name']));

        $element['condition'] = ($rule ? $rule['type'] : false);
        $element['rules'] = ($rule ? $rule['rule'] : false);
        $element['default'] = ($rule ? $rule['default'] : false);

        return $element;
    }

    public static function parseArray($model)
    {
        $values = [];
        $data = $model::all();

        foreach ($data as $element) {
            array_push($values, $element->getAttributes());
        }

        return $values;
    }


    public static function getValue(object $singleElement)
    {
        if (gettype($singleElement->value) === 'array') {
            $singleElement->value = $singleElement->value['text'];
        }

        // Exceldate (yyyy-mm-ddT00:00:00.000Z) check if is date and return to (mm/dd/yyyy)
        switch (true) {
            case (strtolower($singleElement->name) === 'year' && preg_match_all('/(^[0-9]{1,2})(\/{1})(([0-9]{1,2}))(\/{1})(([0-9]{4,4})$)/', $singleElement->value, $matches, PREG_SET_ORDER, 0) === 1):
            case (strtolower($singleElement->name) === 'avabilablestart'):
            case (strtolower($singleElement->name) === 'availableend'):
                $singleElement->value = ((strtotime($singleElement->value) && !ctype_alpha($singleElement->value)) ? date('m/d/yy', strtotime($singleElement->value)) : $singleElement->value);
                break;
        }

        return $singleElement;
    }


    protected static function extractValueFromData($element, $value)
    {
        $element->value = (isset($value['value']) ? $value['value'] : null);
        $element->row_number = $value['row_number'];

        if (gettype($element->value) === 'array') {
            $element->value = self::getValue($element)->value;
        }

        return $element;
    }


    public static function createConnectionToFileServer($username, $password, $host = 'video1.rlaxxtv.com')
    {
        return Storage::build([
            'driver' => 'sftp',
            'host' => $host,
            'username' => $username,
            'password' => $password,

            'privateKey' => 'C:\Users\OmerAlBadry\.ssh\pkey.ppk',
            // Optional SFTP Settings...
            'port' => 22,
            'root' => 'uploads',
        ]);
    }
}
