<?php
namespace App\Services;
use App\Models\Languages;
use App\Models\Countries;

class RequireTableInformationService{

    public static function handleTableRule($name,$data = null){
        //which column is requested!
            //which data will be needed
        if (in_array($name,["language"]))
            $data = ExcelParserService::parseArray(Languages::class);
        elseif (in_array($name,["blockedcountries","country","allowedcountries"]))
            $data = ExcelParserService::parseArray(Countries::class);
        //return needed information
        return $data;
    }
}
