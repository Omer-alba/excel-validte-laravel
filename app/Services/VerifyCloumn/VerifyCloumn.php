<?php

namespace App\Services\VerifyCloumn;

use App\Models\Countries;
use App\Models\Languages;
use App\Services\ExcelParserService;

class VerifyCloumn
{
    protected static $url_fox = 'http://video1.rlaxxtv.com/';

    public function __construct()
    {
        //
    }

    public static function verify_If_Condition(object $column)
    {
        global $errorMsg;

        //Start
        if (gettype($column->rules) === 'array') {
            $column->value = preg_replace('/\s+/', '', $column->value);
            $valueAsArray = explode(',', $column->value);

            //check if value is in rules
            $isAvaible = [];
            foreach ($valueAsArray as $singleValue) {
                foreach ($column->rules as $array) {
                    $lowerCaseArray = array_map('strtolower', $array);

                    if (in_array(strtolower($singleValue), $lowerCaseArray)) {
                        $isAvaible[$singleValue] = true;
                        continue 2;
                    }

                    $isAvaible[$singleValue] = false;
                }
            }

            //catch error und return!
            foreach ($isAvaible as $value) {
                if (!$value) {
                    $errorMsg->assembleErrors($column, 'valueError');

                    if (in_array($column->name, ['language'])) {
                        $errorMsg->collectErrors($column, 'language', Languages::all());
                    } elseif (in_array($column->name, ['allowedCountries', 'country'])){
                        $errorMsg->collectErrors($column, 'country', Countries::all());
                    }

                }
            }

        } elseif ($column->rules === 'enumeration') {

            if (count(explode('/', $column->value)) > 2 || count(explode(':', $column->value)) > 2 || count(explode(';', $column->value)) > 2) {
                $errorMsg->assembleErrors($column, 'wrong delimiter');
            }

        }
    }

    public static function verify_Regex_Condition($element)
    {
        /*
         * $element parameter must be an object!
         * $element all information about which 'sheet name', 'column name', 'description', 'row number', 'column name (alphabet)', 'rules', 'condition', etc..
         */
        global $errorMsg;
        if (
            (strtoupper($element->type) === 'OPTIONAL' && (gettype(ExcelParserService::getValue($element)->value) === 'NULL'))
            ||
            (strtoupper($element->type) === 'REQUIRED' && $element->default && (gettype(ExcelParserService::getValue($element)->value) === 'NULL'))
        ) return true;

        try {
            $result = preg_match($element->rules, ExcelParserService::getValue($element)->value, $output_array);

            if ($element->condition === '!regex') {
                if ($result > 0) {
                    $errorMsg->assembleErrors($element, 'valueError');
                    // if error type is only uppercase
                    if (in_array($element->name, ['seriesTitle', 'title'])) {
                        $errorMsg->collectErrors($element, 'spelling', false);
                    }
                }
            } else {
                if ($result === 0) {
                    $errorMsg->assembleErrors($element, 'valueError');
                }
            }

        } catch (Exception $error) {
            echo 'error : ' . $error->getMessage();
            dump($element);
            $errorMsg->assembleErrors($element, 'valueError');
        }
    }

    public static function verify_url($element, $allFile)
    {
        global $errorMsg;

        //check if first char is "/"
        if (preg_match_all('/^\//', $element->value, $matches, PREG_SET_ORDER, 0) == 1) {
            $element->value = substr($element->value, 1);
        }

        $isAvaible = in_array(rtrim($element->value), $allFile);

        if (!$isAvaible) {
            $errorMsg->assembleErrors($element, 'url is invalid');
            $errorMsg->collectErrors($element, 'invalid', $allFile);
        }
    }

}
