<?php

namespace App\Services\ErrorAssemble;

use App\Models\ErrorSampler;
use App\Models\Languages;

class ErrorMsg
{

    public $errorMessage;
    public $filename;
    public $contentPartnerName;
    public $contentPartnerPassword;
    public $connectionFailed;
    public $userNotExists;

    // Error collector variables
    public $upperCaseError;
    public $collector = array();
    public $diverseCollector = array();
    public $allFile;

    public function __construct()
    {
        $this->errorMessage = [];
        $this->filename = "";
        $this->contentPartnerName = "";
        $this->contentPartnerPassword = "";
        $this->connectionFailed = false;
        $this->userNotExists = false;
        $this->upperCaseError = [];
    }

    public function assembleErrors(object $element, $errorType)
    {
        // Errormessage = whats wrong where and which :name($element->name) :sheet_name($element->sheet_name)
        switch ($errorType) {
            // Fatal Errors which will stop the App and return the Message!
            case 'double cast':
                $fatalError = [
                    'message' => "Double name assignment please check the name = <b style='font-size: large'>$element->name</b> <span class='badge badge-secondary'>Sheetname : $element->sheet_name</span>",
                    'class' => 'alert-danger'
                ];
                $this->attendArray((object)$fatalError);
                return $this->showErrorMessageToClient();
                break;
            case 'sheetname is missing':
                $fatalError = [
                    'message' => "Sheetname is missing please check your Excelfile! <span class='badge badge-secondary'>Sheetname : $element->sheet_name</span>",
                    'class' => 'alert-danger'
                ];
                $this->attendArray((object)$fatalError);
                $this->showErrorMessageToClient();
                break;
            case 'rows is missing':
                $fatalError = [
                    'message' => "Rows is missing name = <b style='font-size: large'>$element->name</b>. <span class='badge badge-secondary'>Sheetname : $element->sheet_name </span>",
                    'class' => "alert-danger"
                ];
                $this->attendArray((object)$fatalError);
                $this->showErrorMessageToClient();
                break;
            case 'Type is missing':
                $fatalError = [
                    'message' => "Type is missing please check the Value of <b style='font-size: large'>$element->name</b>. <span class='badge badge-secondary'>Sheetname : $element->sheet_name | Row number: $element->row_number</span>",
                    'class' => 'alert-danger'
                ];
                $this->attendArray((object)$fatalError);
                $this->showErrorMessageToClient();
                break;
            //<----- Error that are not stopping the app, belongs down ----->
            case 'fatal':
            case 'required value missing':
                $htmlArray = ["Required Element is missing please check the Value of <b style='font-size: large'>$element->name</b>.
                    <span class='badge badge-secondary'>Sheetname : $element->sheet_name |
                    Row number: $element->row_number |
                    Column: $element->column
                    </span>"];
                if (in_array($element->name, [
                    'seriesSeasonNumber',
                    'seriesEpisodeNumber',
                    'language',
                    'country',
                    'title',
                    'description',
                    'seriesTitle',
                    'seriesDescription',
                    'videoFile',
                    'thumbnail',
                    'seriesThumbnail',
                    'seriesVideoFil'
                ])
                ) {
                    array_push($htmlArray, "<a class='btn btn-outline-secondary' href='/error-sampler/{$this->contentPartnerName}/diverse/missing-value'>fix all</a>");
                }
                $fatalError = [
                    'message' => implode(' ',$htmlArray),
                    'class' => 'alert-danger'
                ];
                $this->attendArray((object)$fatalError);
                break;
            case 'valueError':
                $html_array = ["
                    Required Element has wrong value please check the Value of <b style='font-size: large'>$element->name</b>.
                        <span class='badge badge-secondary'>Sheetname : $element->sheet_name |
                            Row number: $element->row_number |
                            Column: $element->column</span>
                            <br/>
                            <hr/>
                            <h6>Passed Value</h6>
                            <div style='background-color: ghostwhite; padding: 10px; font-style: italic; font-size: small;'>$element->value</div>
                            <hr/>
                            <h6><span style='font-size: larger; color: #857b26;'>Tipp</span>, please read the description carefully!</h6>
                            <div style='background-color: ghostwhite; padding: 10px; font-style: italic; font-size: small;'>$element->description</div>
                            <br />
                        </span>
                "];
                if (in_array($element->name,[
                    'seriesSeasonNumber',
                    'seriesEpisodeNumber',
                    'language',
                    'country',
                    'title',
                    'description',
                    'seriesTitle',
                    'seriesDescription',
                    'videoFile',
                    'thumbnail',
                    'seriesThumbnail',
                    'seriesVideoFil'
                ])
                ){
                    array_push($html_array,
                        "<a class='btn btn-outline-secondary' href='/error-sampler/{$this->contentPartnerName}/{$this->getErrorType($element->name)}'>fix all</a>");
                }
                $error = [
                    'message' => implode(' ',$html_array),
                    'class' => 'alert-warning'
                ];
                $this->attendArray((object)$error);
                break;
            case "url is invalid":
                $element->value = (isset($element->url) ? $element->url : $element->value);
                $error = [
                    'message' => <<<EDO
                        the url you entered is not Valid please check
                        <u>url</u> of
                        <b style='font-size: large'>$element->name</b>.
                        <span class='badge badge-secondary'> Sheetname: $element->sheet_name | Row number: $element->row_number | Column: $element->column </span>
                        <br/>
                        <hr/>
                        <h6>Passed Value</h6>
                        <div style='background-color: ghostwhite; padding: 10px; font-style: italic; font-size: small;'>
                            $element->value
                         </div>
                         <hr/>
                        <h6><span style="font-size: larger; color: #857b26;">Tipp</span>, please read the description carefully!</h6>
                        <div style='background-color: ghostwhite; padding: 10px; font-style: italic; font-size: small;'>$element->description</div>
                        <br/>
                        <a class='btn btn-outline-secondary' href='/error-sampler/{$this->contentPartnerName}/{$this->getErrorType($element->name)}'>fix all</a>

                    EDO,
                    'class' => 'alert-warning'
                ];
                $this->attendArray((object)$error);
                break;
            case 'mime type Error':
                $error = [
                    'message' => "Wrong Mime-Type Error please check the <u>value</u> <b style='font-size: large'>$element->name</b> needs an differnt Mime-Type. <span class='badge badge-secondary'>Sheetname : $element->sheet_name |´Row number: $element->row_number</span><br/>",
                    'class' => 'alert-warning'
                ];
                $this->attendArray((object)$error);
                break;
            case 'attributemissing':
                $error = [
                    'message' => "Missing Attribute the <u>value</u> <b style='font-size: large'>$element->name</b>. <span class='badge badge-secondary'>Sheetname : $element->sheet_name | Row number: $element->row_number</span><br/>",
                    'class' => 'alert-info'
                ];
                $this->attendArray((object)$error);
                break;
            case 'wrong delimiter':
                $error = [
                    'message' => "Delimiter is mixed or wrong, the <u>value</u> <b style='font-size: large'>$element->name</b>Sheetname : $element->sheet_name | Row number: $element->row_number</span><br/>",
                    'class' => 'alert-info'
                ];
            //next case...
        }
        return false;
    }

    private function attendArray(object $error)
    {
        array_push($this->errorMessage, $error);
    }

    public function showErrorMessageToClient()
    {
        return view('error.index')
            ->with([
                'errors' => $this->errorMessage,
                'filename' => $this->filename
            ]);
    }

    public function collectUpperCaseError($element)
    {
        // http://127.0.0.1:3002/writeExcel/lowerCase?worksheetName={$element->sheet_name}&row={$element->row_number}&cell={$element->column}&content={$element->value}&filename={$this->filename}
        array_push($this->upperCaseError, [
            'url' => 'http://127.0.0.1:3001/writeExcel/lowerCase',
            'content' => [
                'cell' => $element->column,
                'content' => $element->value,
                'row' => $element->row_number,
                'worksheetName' => $element->sheet_name,
                'description' => $element->description,
                'status' => [
                    'message' => '',
                    'successful' => ''
                ]
            ],
            'filename' => $this->filename
        ]);
    }

    public function collectErrors($element, $kindOfError, $requires = false)
    {
        if (!isset($this->collector[$kindOfError])) {
            $this->collector[$kindOfError] = array();

            $sampler = array();
            array_push($sampler, ['content' => json_encode([])]);

            if ($requires) {
                array_push($sampler, ['requires' => json_encode($requires)]);
            }

            ErrorSampler::updateOrCreate(
                ['name' => $this->contentPartnerName . '-' . $kindOfError],
                ['requires' => json_encode($requires), 'url' => $this->getErrorType($element->name), 'content' => json_encode([])]
            );
        }

        array_push($this->collector[$kindOfError], [
            'content' => [
                'cell' => $element->column,
                'content' => $element->value,
                'row' => $element->row_number,
                'worksheetName' => $element->sheet_name,
                'description' => $element->description,
                'status' => [
                    'message' => '',
                    'successful' => ''
                ]
            ],
            'filename' => $this->filename
        ]);
    }

    public function getErrorType($elementName)
    {
        switch ($elementName) {
            case 'seriesTitle':
            case 'title':
            case 'actors':
                return 'spelling';
                break;
            case 'language':
                return 'language';
                break;
            case 'seriesThumbnail':
            case 'videoFile':
            case 'thumbnail':
                return 'invalid';
                break;
            case  'country':
                return 'country';
                break;
        }
    }

    public function collectErrorsDivers($element)
    {
        switch ($element->name) {
            case 'seriesSeasonNumber':
            case 'seriesEpisodeNumber':
                if (!isset($this->diverseCollector['number'])) {
                    $this->diverseCollector['number'] = array();
                    $this->diverseCollector['number']['resolve'] = 'number';
                    $this->diverseCollector['number']['elements'] = array();
                    $this->diverseCollector['number']['filename'] = $this->filename;
                }
                array_push($this->diverseCollector['number']['elements'], $this->storeErrors($element, 'number-value'));
                break;
            case 'language':
            case 'country':
                if (!isset($this->diverseCollector['language'])) {
                    $this->diverseCollector['language'] = array();
                    $this->diverseCollector['language']['resolve'] = Languages::all();
                    $this->diverseCollector['language']['elements'] = array();
                    $this->diverseCollector['language']['filename'] = $this->filename;
                }
                array_push($this->diverseCollector['language']['elements'], $this->storeErrors($element, 'selected-language'));
                break;
            case 'title':
            case 'description':
            case 'seriesDescription':
            case 'seriesTitle':
                if (!isset($this->diverseCollector['text'])) {
                    $this->diverseCollector['text'] = array();
                    $this->diverseCollector['text']['resolve'] = 'text';
                    $this->diverseCollector['text']['elements'] = array();
                    $this->diverseCollector['text']['filename'] = $this->filename;
                }
                array_push($this->diverseCollector['text']['elements'], $this->storeErrors($element, 'text-value'));
                break;
            case 'videoFile':
            case 'thumbnail':
            case 'seriesThumbnail':
            case 'seriesVideoFil':
                if (!isset($this->diverseCollector['files'])) {
                    $this->diverseCollector['files'] = array();
                    $this->diverseCollector['files']['resolve'] = $this->allFile;
                    $this->diverseCollector['files']['elements'] = array();
                    $this->diverseCollector['files']['filename'] = $this->filename;
                }
                array_push($this->diverseCollector['files']['elements'], $this->storeErrors($element, 'selected-value'));
                break;
        }
    }

    public function storeErrors($element, $form_name)
    {
        return [
            'content' => [
                'name' => $element->name,
                'cell' => $element->column,
                'content' => $element->value,
                'row' => $element->row_number,
                'worksheetName' => $element->sheet_name,
                'description' => $element->description,
                'status' => [
                    'message' => '',
                    'successful' => ''
                ]
            ],
            'form_name' => $form_name,
            'filename' => $this->filename,
        ];
    }

}
