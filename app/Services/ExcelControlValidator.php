<?php

namespace App\Services;

//own
use App\Services\Rows\Rows;
use App\Services\VerifyCloumn\VerifyCloumn;
use App\Services\ExcelParserService;

class ExcelControlValidator
{
    private $sheet_name;

    public function __constructor()
    {
        $this->sheet_name = '';
    }

    public function isRowAvaible(object $data)
    {
        global $errorMsg;
        // get all rows which must be in the excel File.
        $rows = Rows::requiredRows();

        // create new Array which contains only the <Key> as sheet_name and an <Array> with all $column_names from the Parameter
        $sheetNameArray = [];

        foreach ($data as $singleObject) {
            $singleObject = (object)$singleObject;

            try {
                if (!array_key_exists($singleObject->sheet_name, $sheetNameArray)) {
                    $sheetNameArray[$singleObject->sheet_name] = [];
                }
            } catch (\Exception $e) {
                echo 'Error!';
                dump($singleObject);
                dump($e);
            }

            if (array_search($singleObject->name, $sheetNameArray[$singleObject->sheet_name])) {
                //Fatal error (double cast!)
                return $errorMsg->assembleErrors($singleObject, 'double cast');
            }

            // push new column name to array!
            array_push($sheetNameArray[$singleObject->sheet_name], $singleObject->name);
        }

        //compare now all local->$rows with file->$rows
        foreach ($rows as $rowKey => $rowArray) {
            if (!array_key_exists($rowKey, $sheetNameArray))
                return $errorMsg->assembleErrors((object)['sheet_name' => $rowKey], 'sheetname is missing');
            // if true app will die with error!

            foreach ($rowArray as $rowColumnName) {
                // check if sheetname is avaible
                if (!in_array($rowColumnName, $sheetNameArray[$rowKey]))
                    return $errorMsg->assembleErrors((object)['name' => $rowColumnName, 'sheet_name' => $rowKey], 'rows is missing');
            }
        }
    }

    public static function isRequiredSetted($element, $allFile)
    {
        global $errorMsg;

        if (!isset($element->name)) {
            return $errorMsg->assembleErrors((object)$element, 'required value missing');
        }

        if (!$element->default && ExcelParserService::getValue($element)->value === null) {
            $errorMsg->assembleErrors((object)$element, 'required value missing');
            $errorMsg->collectErrorsDivers((object)$element);
            return false;
        }

        switch ($element->condition) {
            case 'regex':
            case '!regex':
                VerifyCloumn::verify_Regex_Condition($element);
                break;
            case 'url':
                VerifyCloumn::verify_url($element, $allFile);
                break;
            case 'if':
                VerifyCloumn::verify_If_Condition($element);
                break;
        }

        return true;
    }


    public function isOptionalSetted(object $element, $allFile)
    {
        global $errorMsg;

        if (empty($element->value)) {
            return false;
        }

        if (!$element->condition || !$element->rules || !$element->name || (empty($element->value) && strtolower($element->type) !== 'optional')) {
            $errorMsg->assembleErrors($element, 'attributemissing');
            return false;
        }

        switch ($element->condition) {
            case 'regex':
            case '!regex':
                VerifyCloumn::verify_Regex_Condition($element);
                break;
            case 'url':
                VerifyCloumn::verify_url($element, $allFile);
                break;
            case 'if':
                VerifyCloumn::verify_If_Condition($element);
                break;
        }
    }

}
